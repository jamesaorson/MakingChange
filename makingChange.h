#ifndef MAKING_CHANGE
#define MAKING_CHANGE

//Holds how many of each coin have been used and the total number of coins used
struct solutionData {
    int* coins;
    int coinsUsed;
};

//Performs the bottom up solution for making change
void bottomUp(std::vector<int>* denominations, int value, solutionData* memo);

//Loops through and cleans up memory
void cleanUpMemo(solutionData* memo, int size);

//Returns a deep copy of the array
int* copyArray(int* array, int size);

//Wrapper function for bottomUp() which will call printSolution()
void makeChangeBottomUp(std::vector<int>* denominations,
                        int value,
                        std::ofstream* output);

//Wrapper function for recursiveMemo() which will call printSolution()
void makeChangeRecursiveMemo(std::vector<int>* denominations,
                             int value,
                             std::ofstream* output);

//Wrapper function for recursiveNoMemo() which will call printSolution()
void makeChangeRecursiveNoMemo(std::vector<int>* denominations,
                               int value,
                               std::ofstream* output);

//Reads the flag and performs the passed function
void performFlagAction(std::string flag,
                       std::vector<int>* denominations, 
                       std::vector<int>* problems,
                       std::ofstream* output);

//Loops through all problems and calls the passed function on each problem
void performMakeChange(std::vector<int>* denominations,
                       std::vector<int>* problems,
                       std::ofstream* output,
                       void (*makeChange) (std::vector<int>* denominations,
                                           int value,
                                           std::ofstream* output));

//Prints solution to the specified ofstream
void printSolution(solutionData* solution,
                 std::vector<int>* denominations,
                 int value,
                 double time,
                 std::ofstream* output);

//Performs the recursive with memoization solution for making change
void recursiveMemo(std::vector<int>* denominations,
                   int value,
                   solutionData* memo);

//Performs the recursive without memoization solution for making change
solutionData recursiveNoMemo(std::vector<int>* denominations,
                             int value,
                             solutionData currentBestSolution);

#endif //MAKING_CHANGE
